import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import AppRoute from './routes/AppRoute'
import Toast from './components/Toast'
import 'react-toastify/dist/ReactToastify.css'
import LandingPage from './views/LandingPage'
import NewGame from './views/NewGame'
import Error from './views/Error'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <AppRoute path="/" exact component={LandingPage} />
        <AppRoute path="/game" exact component={NewGame} />

        <Route path="/error" component={Error} />
        <Route path="*" component={Error} />
      </Switch>
      <Toast />
    </BrowserRouter>
  )
}

export default App
