import React from 'react'
import './loading.css'

const Loading = () => {
  return (
    <div className="loading">
      <img src={'/images/loader.gif'} alt="loading" />
    </div>
  )
}
export default Loading
