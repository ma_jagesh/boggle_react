import React, { useState, useEffect } from 'react'
import { calcTimeLeft } from '../utils/CounterUtils'
import { PropTypes } from 'prop-types'

const Counter = ({ updateCallback }) => {
  const [timeLeft, setTimeLeft] = useState(calcTimeLeft(180000))
  const [counter, setCounter] = useState(180000)

  useEffect(() => {
    const id = setInterval(() => {
      if (counter > 0) {
        setTimeLeft(calcTimeLeft(counter - 1000))
        setCounter(counter - 1000)
        updateCallback(counter)
      } else {
        setCounter(0)
        setTimeout(() => updateCallback(0), 500)
      }
    }, 1000)
    return () => clearInterval(id)
  }, [counter, updateCallback])

  return (
    <div className="row">
      <div className="text-warning">
        {timeLeft.minutes} : {timeLeft.seconds}
      </div>
      {counter > 0 ? '' : <div className="ml-3">Times up!!</div>}
    </div>
  )
}

Counter.propTypes = {
  updateCallback: PropTypes.func.isRequired,
}

export default Counter
