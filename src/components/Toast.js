import React from 'react'
import { ToastContainer, toast } from 'react-toastify'

const Toast = () => {
  return (
    <ToastContainer
      hideProgressBar
      autoClose={4000}
      newestOnTop={true}
      draggable={false}
      pauseOnHover
      position={toast.POSITION.BOTTOM_RIGHT}
    />
  )
}

export default Toast
