import React from 'react'
import { PropTypes } from 'prop-types'

const Summary = ({ score, words }) => {
  let longestWord = ''
  words.forEach((c) => {
    longestWord = c.length >= longestWord.length ? c : longestWord
  })

  return (
    <div data-test="SummaryComponent" className="col-6 center mt-6">
      <div className="card bg-secondary shadow">
        <div className="card-header border-0">
          <h2>Summary</h2>
        </div>
        <div className="card-body">
          <div className="row word-list-item">
            <p>Longest Word</p>
            <div data-test="longestWord" className="col text-right text-uppercase">
              {longestWord}
            </div>
          </div>
          <div className="row word-list-item mt-4">
            <p>Total Words</p>
            <div className="col text-right">{words.length}</div>
          </div>
          <div className="row word-list-item mt-4">
            <p>Your Score</p>
            <div className="col text-right">{score}</div>
          </div>
        </div>
      </div>
    </div>
  )
}

Summary.propTypes = {
  score: PropTypes.number.isRequired,
  words: PropTypes.array.isRequired,
}

export default Summary
