import useNetworkDetector from '../hooks/useNetworkDetector'
import React from 'react'
import { PropTypes } from 'prop-types'

const AppLayout = ({ children, ...props }) => {
  const { isConnected, serverStatus } = useNetworkDetector()

  return (
    <section>
      {!isConnected && (
        <div className="alert alert-danger" role="alert">
          Not connected to internet
        </div>
      )}
      {!serverStatus && isConnected && (
        <div className="alert alert-danger" role="alert">
          Server is offline
        </div>
      )}
      {children}
    </section>
  )
}
AppLayout.propTypes = {
  children: PropTypes.object.isRequired,
}
export default React.memo(AppLayout)
