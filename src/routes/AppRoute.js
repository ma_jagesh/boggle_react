import React from 'react'
import { Route, useHistory, Redirect } from 'react-router-dom'

import AppLayout from '../layouts/index'
import PropTypes from 'prop-types'

const AppRoute = ({ component: Component, ...props }) => {
  const history = useHistory()
  if (history.location.pathname === '/game' && !history.location.state) {
    return (
      <Redirect
        to={{
          pathname: '/',
        }}
      />
    )
  }

  return (
    <Route
      {...props}
      render={(props) => {
        return (
          <AppLayout>
            <Component {...props} />
          </AppLayout>
        )
      }}
    />
  )
}

AppRoute.propTypes = {
  component: PropTypes.elementType.isRequired,
}
export default AppRoute
