import Axios from 'axios'
import { apiEnd, apiStart, apiError, serverStatus } from '../store/general/actions'
import { API } from '../store/general/types'
import { successNotify, failureNotify } from '../utils/Notify'

const apiMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action)

  if (action.type !== API) return

  const { url, method, data, onSuccess, onFailure, label, headers } = action.payload

  const dataOrParams = ['GET', 'DELETE'].includes(method) ? 'params' : 'data'

  const axios = Axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    timeout: 5000,
  })

  axios.interceptors.response.use(
    (response) => {
      if (response.config.method !== 'get' && response.data.message) {
        successNotify(response.data.message)
      }
      return response
    },
    (error) => {
      if (!error.response) {
        dispatch(serverStatus(false))
      } else if (error.response.data.message) {
        failureNotify(error.response.data.message)
      }
      return Promise.reject(error)
    },
  )

  if (label) {
    dispatch(apiStart(label))
  }

  axios
    .request({
      url,
      method,
      headers,
      [dataOrParams]: data,
    })
    .then(({ data }) => {
      dispatch(serverStatus(true))
      if (data.metadata) {
        onSuccess({
          data: data.data,
        })
      } else {
        onSuccess(data.data, dispatch)
      }
    })
    .catch((error) => {
      dispatch(apiError(error))
      if (error.response) {
        onFailure(error.response.data.errors)
      }

      if (error.response && (error.response.status === 403 || error.response.status === 404)) {
        onFailure(error.response.data.message)
      }
    })
    .finally(() => {
      if (label) {
        dispatch(apiEnd(label))
      }
    })
}

export default apiMiddleware
