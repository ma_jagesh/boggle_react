import { useCallback, useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'

const useNetworkDetector = () => {
  const [isConnected, setIsConnected] = useState(true)
  const serverStatus = useCallback(useSelector((state) => state.api.isServerOnline))

  function webPing() {
    setInterval(() => {
      fetch('//google.com', {
        mode: 'no-cors',
      })
        .then(() => {
          setIsConnected(true)
        })
        .catch(() => setIsConnected(false))
    }, 2000)
  }

  const pingRef = useRef(webPing)

  const handleConnectionChange = () => {
    const condition = navigator.onLine ? 'online' : 'offline'

    if (condition === 'online') {
      webPing()
      return
    }
    return setIsConnected(false)
  }

  useEffect(() => {
    window.addEventListener('online', handleConnectionChange)
    window.addEventListener('offline', handleConnectionChange)

    return () => {
      clearTimeout(pingRef)
      window.removeEventListener('online', handleConnectionChange)
      window.removeEventListener('offline', handleConnectionChange)
    }
  }, [])

  return { isConnected, serverStatus }
}

export default useNetworkDetector
