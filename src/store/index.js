import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import apiMiddleware from '../middleware/api'
import reducers from './reducers'

const store = createStore(reducers, {}, applyMiddleware(thunk, apiMiddleware))

export default store
