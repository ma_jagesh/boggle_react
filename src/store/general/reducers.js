import { ACCESS_DENIED, API_END, API_ERROR, API_START, SERVER_STATUS } from './types'

const initialState = {
  isLoadingData: false,
  errors: {},
  isServerOnline: true,
}

const ApiReducer = (state = initialState, actions) => {
  switch (actions.type) {
    case SERVER_STATUS:
      return {
        ...state,
        isServerOnline: actions.payload,
      }
    case API_START:
      return {
        ...state,
        isLoadingData: true,
      }
    case API_END:
      return {
        ...state,
        isLoadingData: false,
      }
    case API_ERROR:
      return {
        ...state,
        errors: actions.payload,
      }
    case ACCESS_DENIED:
      return {
        ...state,
        ...actions.payload,
      }
    default:
      return state
  }
}

export default ApiReducer
