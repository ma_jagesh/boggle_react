import { combineReducers } from 'redux'
import ApiReducer from './general/reducers'

const reducers = combineReducers({
  api: ApiReducer,
})

export default reducers
