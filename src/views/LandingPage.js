import React, { useEffect } from 'react'
import { useImmerReducer } from 'use-immer'
import { fetchPlayers, requestPlayerCreate } from '../services/landingServices'
import { useDispatch, useSelector } from 'react-redux'
import Loading from '../components/loading'
import './views.css'
import { PropTypes } from 'prop-types'

const FETCH_SUCCESS = 'FETCH_SUCCESS'
const FETCH_ERROR = 'FETCH_ERROR'
const INPUT_CHANGE = 'INPUT_CHANGE'
const UPDATE_ERRORS = 'UPDATE_ERRORS'

const initialState = {
  listData: [],
  player: {},
  formError: '',
  fetchError: '',
}

const reducer = (draft, action) => {
  switch (action.type) {
    case FETCH_SUCCESS: {
      var dataRecords = action.payload
      dataRecords.map(
        (record) => (record.playedAt = new Date(record.playedAt + ' UTC').toLocaleString('en-US')),
      )
      draft.listData = action.payload
      draft.fetchError = ''
      break
    }
    case FETCH_ERROR: {
      draft.fetchError = action.payload
      break
    }
    case INPUT_CHANGE: {
      draft.player.name = action.payload
      break
    }
    case UPDATE_ERRORS:
      draft.formError = action.payload
      break
    default:
      break
  }
}

const LandingPage = ({ history }) => {
  const rootDispatch = useDispatch()
  const loading = useSelector((state) => state.api.isLoadingData)

  const [state, dispatch] = useImmerReducer(reducer, initialState)

  useEffect(() => {
    rootDispatch(
      fetchPlayers(
        history,
        (data) => {
          dispatch({ type: FETCH_SUCCESS, payload: data })
        },
        (error) => {
          dispatch({ type: FETCH_ERROR, payload: error })
        },
      ),
    )
  }, [])

  const handleChange = (evt) => {
    dispatch({
      type: INPUT_CHANGE,
      payload: evt.target.value,
    })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    e.stopPropagation()

    if (Object.keys(state.player).length === 0) {
      dispatch({
        type: UPDATE_ERRORS,
        payload: 'Name cannot be empty!',
      })
      return
    }

    rootDispatch(
      requestPlayerCreate(state.player, history, (player) => {
        history.push({
          pathname: '/game',
          state: { playerId: player.id },
        })
      }),
    )
  }

  const startGame = (id) => {
    history.push({
      pathname: '/game',
      state: { playerId: id },
    })
  }

  return (
    <React.Fragment>
      {loading ? <Loading /> : ''}

      <div data-test="landingPage" className="container-fluid">
        <div className="row mb-6 mt-6">
          <div className="col-xl-8 mb-5 mb-xl-0">
            <div className="card shadow">
              <div className="card-header border-0">
                <div className="row align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Players</h3>
                  </div>
                </div>
              </div>
              <div data-test="playersTable" className="table-responsive">
                <table className="table align-items-center table-flush">
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Games Played</th>
                      <th scope="col">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {state.listData.length === 0 ? (
                      <tr>
                        <td>{state.fetchEror}</td>
                      </tr>
                    ) : (
                      state.listData.map((player) => (
                        <tr test-data="playerRow" key={player.id}>
                          <th scope="row">{player.name}</th>
                          <td>{player.gamesPlayed}</td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-sm"
                              onClick={() => startGame(player.id)}
                            >
                              <i className="fa fa-play" />
                            </button>
                            <button
                              type="button"
                              className="btn btn-sm"
                              onClick={() => console.log('edit clicked')}
                            >
                              <i className="fa fa-edit" />
                            </button>
                            <button
                              type="button"
                              className="btn btn-sm"
                              onClick={() => console.log('delete clicked')}
                            >
                              <i className="fa fa-trash" />
                            </button>
                          </td>
                        </tr>
                      ))
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card bg-secondary shadow">
              <div className="card-header border-0">
                <div className="row align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Begin here</h3>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <form data-test="playerForm" onSubmit={handleSubmit}>
                  <div className="form-group focused">
                    <label className="form-control-label" htmlFor="input-name">
                      Your name
                    </label>
                    <input
                      data-test="playerNameInput"
                      id="input-name"
                      type="text"
                      autoComplete="off"
                      pattern="[A-Za-z]{2,}"
                      title="Only letters are allowed, at least two."
                      className="form-control form-control-alternative"
                      placeholder="John Doe"
                      onChange={handleChange}
                    />
                    {state.formError ? <p className="error-msg">{state.formError}</p> : ''}
                  </div>
                  <div className="text-center">
                    <button
                      data-test="startGameButton"
                      type="submit"
                      className="btn btn-primary my-4"
                    >
                      Start game
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

LandingPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }),
}

export default LandingPage
