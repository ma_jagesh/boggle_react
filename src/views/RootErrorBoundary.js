import React from 'react'
import PropTypes from 'prop-types'

export class RootErrorBoundary extends React.Component {
  constructor(props) {
    super(props)
    this.state = { hasError: false, error: null, errorInfo: null }
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ hasError: true, error, errorInfo })
  }

  render() {
    if (process.env.NODE_ENV === 'development' && this.state.hasError)
      console.error(this.state.error)

    if (this.state.hasError) {
      return (
        <div className="error-wrapper p-2">
          <div className="card shadow-sm p-5 border-0 rounded-0">
            <div className="text-wrapper mx-auto text-right">
              <h1 className="mb-2">Oops!!</h1>
              <p className="fs-18 mb-2">An error occurred in the application.</p>
              <p className="fs-18 mb-0">
                Try navigating to&nbsp;
                <a className="text-underline text-theme-blue" href="/">
                  <strong>Home</strong>.
                </a>
              </p>
            </div>
          </div>
        </div>
      )
    }
    return this.props.children
  }

  static propTypes = {
    children: PropTypes.node.isRequired,
  }
}
