import React, { useEffect } from 'react'
import { useImmerReducer } from 'use-immer'
import Loading from '../components/loading'
import {
  requestNewGame,
  requestWordValidation,
  submitGameSummary,
} from '../services/gameServices.js'
import { useDispatch, useSelector } from 'react-redux'
import './views.css'
import { CreateGridHtml, isValidInGrid } from '../utils/GridUtils'
import Parser from 'html-react-parser'
import { PropTypes } from 'prop-types'
import Counter from '../components/Counter'
import Summary from '../components/Summary'

const FETCH_SUCCESS = 'FETCH_SUCCESS'
const INPUT_CHANGE = 'INPUT_CHANGE'
const UPDATE_ERRORS = 'UPDATE_ERRORS'
const UPDATE_WORD_LIST = 'UPDATE_WORD_LIST'
const SHOW_SUMMARY = 'SHOW_SUMMARY'

const initialState = {
  player: {},
  score: 0,
  word_list: [],
  boardTiles: '',
  matrix: {},
  current_word: '',
  formError: '',
  showSummary: false,
}

const reducer = (draft, action) => {
  switch (action.type) {
    case FETCH_SUCCESS: {
      draft.player = action.payload.player
      const [tiles, matrix] = CreateGridHtml(4, 4, action.payload.letters)
      draft.boardTiles = tiles
      draft.matrix = matrix
      break
    }
    case INPUT_CHANGE: {
      draft.current_word = action.payload
      break
    }
    case UPDATE_ERRORS:
      draft.formError = action.payload
      draft.current_word = ''
      break
    case UPDATE_WORD_LIST:
      draft.word_list.push(action.payload)
      draft.word_count += 1
      draft.current_word = ''
      draft.formError = ''
      draft.score += action.payload.length
      break
    case SHOW_SUMMARY:
      draft.showSummary = action.payload
      break
    default:
      break
  }
}

const NewGame = ({ history }) => {
  const rootDispatch = useDispatch()
  const loading = useSelector((state) => state.api.isLoadingData)
  const [state, dispatch] = useImmerReducer(reducer, initialState)

  const playerId = history.location.state.playerId
  useEffect(() => {
    if (playerId) {
      rootDispatch(
        requestNewGame(
          playerId,
          history,
          (game) => {
            dispatch({
              type: FETCH_SUCCESS,
              payload: game,
            })
          },
          () => {
            history.replace('/game', null)
          },
        ),
      )
    }
  }, [])

  const handleChange = (evt) => {
    dispatch({
      type: INPUT_CHANGE,
      payload: evt.target.value,
    })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    e.stopPropagation()

    if (!state.current_word) {
      dispatch({
        type: UPDATE_ERRORS,
        payload: 'Word cannot be empty!',
      })
      return
    }

    const isValid = isValidInGrid(state.matrix, state.current_word.toUpperCase())
    if (!isValid) {
      dispatch({
        type: UPDATE_ERRORS,
        payload: 'Word does not follow valid path.',
      })
      return
    }

    if (state.word_list.includes(state.current_word)) {
      dispatch({
        type: UPDATE_ERRORS,
        payload: 'Word is already discovered.',
      })
      return
    }

    requestWordValidation(
      state.current_word,
      (res) => {
        if (res && !state.showSummary) {
          dispatch({
            type: UPDATE_WORD_LIST,
            payload: state.current_word,
          })
        } else {
          dispatch({
            type: UPDATE_ERRORS,
            payload: 'Not a valid word.',
          })
        }
      },
      (err) => {
        console.log(err)
      },
    )
  }

  const getCounterUpdate = (val) => {
    if (val === 0) {
      showSummary()
    }
  }

  const cancelClicked = () => {
    if (state.word_list.length > 0) {
      showSummary()
    } else {
      history.replace('/game', null)
    }
  }

  const homeClicked = () => {
    history.replace('/game', null)
  }

  const showSummary = () => {
    const gameData = {
      score: state.score,
      words_count: state.word_list.length,
      player_id: state.player.id,
    }
    rootDispatch(
      submitGameSummary(gameData, history, () => {
        dispatch({
          type: SHOW_SUMMARY,
          payload: true,
        })
      }),
    )
  }

  return (
    <React.Fragment>
      {loading ? (
        <Loading />
      ) : (
        <div data-test="gameComponent" className="container-fluid">
          <div className="row align-items-center">
            <h3 className="mt-2 text-uppercase display-2">Hello {state.player.name}!</h3>
            {state.showSummary ? (
              <div className="col text-right">
                <button type="button" className="btn btn-sm btn-default" onClick={homeClicked}>
                  <i className="fa fa-home" />
                </button>
              </div>
            ) : (
              <div className="col text-right">
                <button type="button" className="btn btn-sm btn-default" onClick={cancelClicked}>
                  <i className="fa fa-times-circle" />
                </button>
              </div>
            )}
          </div>
          {state.showSummary ? (
            <Summary score={state.score} words={state.word_list} />
          ) : (
            <div className="row mt-2">
              <div className="col-xl-8 mb-5 mb-xl-0">
                <div className="row m-1 text-center">
                  <div className="icon ml--3">
                    <i className="fa fa-stopwatch-20 mr-1" />
                  </div>
                  <Counter updateCallback={getCounterUpdate} />
                </div>
                <div className="card bg-secondary shadow">
                  <div className="card-body">
                    <div className="row">
                      <div className="board m-4">
                        {state.boardTiles ? Parser(state.boardTiles) : ''}
                      </div>
                      <div className="col-5">
                        <div className="mt-4 text-center">
                          <form onSubmit={handleSubmit}>
                            <div className="form-group focused">
                              <label className="form-control-label" htmlFor="input-name">
                                Your word
                              </label>
                              <input
                                id="input-name"
                                type="text"
                                autoComplete="off"
                                pattern="[A-Za-z]{2,}"
                                title="at least two letters"
                                value={state.current_word}
                                className="form-control form-control-alternative"
                                onChange={handleChange}
                              />
                              {state.formError ? (
                                <p className="error-msg">{state.formError}</p>
                              ) : (
                                ''
                              )}
                            </div>
                            <div className="text-center">
                              <button type="submit" className="btn btn-primary my-4">
                                Submit
                              </button>
                            </div>
                          </form>
                        </div>
                        {state.word_list.length > 0 ? (
                          <div className="text-center rcorners">
                            <p className="text-bg-secondary">
                              <strong>Score</strong>
                            </p>
                            <div className="score mt--3 text-bg-secondary">{state.score}</div>
                          </div>
                        ) : (
                          ''
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {state.word_list.length > 0 ? (
                <div className="col-xl-4 mt-words-list">
                  <div className="card bg-secondary shadow">
                    <div className="card-header border-0">
                      <div className="row align-items-center">
                        <div className="col">
                          <h3 className="mb-0">List of words</h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        {state.word_list.map((word) => (
                          <p key={word} className="word-list-item">
                            {word.toUpperCase()}
                          </p>
                        ))}
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                ''
              )}
            </div>
          )}
        </div>
      )}
    </React.Fragment>
  )
}

NewGame.propTypes = {
  history: PropTypes.shape({
    location: PropTypes.shape({
      state: PropTypes.shape({
        playerId: PropTypes.number,
      }),
    }),
    replace: PropTypes.func,
  }),
}

export default NewGame
