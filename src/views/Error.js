import React from 'react'
import { Link } from 'react-router-dom'

const Error = () => {
  return (
    <div className="p-7">
      <div className="card card-stats">
        <div className="card-body text-right">
          <div className="row">
            <div className="col">
              <h5 className="card-title text-uppercase text-muted display-2 mb-0">Oops!</h5>
              <span className="h2 font-weight-bold mb-0">
                The page you are trying to find is not available.
              </span>
            </div>
          </div>
          <p className="mt-3 mb-0 text-sm">
            Try navigating to&nbsp;
            <Link className="text-underline text-theme-blue" to="/">
              <strong>Home</strong>
            </Link>
          </p>
        </div>
      </div>
    </div>
  )
}
export default React.memo(Error)
