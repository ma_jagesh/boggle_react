import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { Provider } from 'react-redux'
import store from './store/index'
import { RootErrorBoundary } from './views/RootErrorBoundary'

ReactDOM.render(
  <Provider store={store}>
    <RootErrorBoundary>
      <App />
    </RootErrorBoundary>
  </Provider>,
  document.getElementById('root'),
)
