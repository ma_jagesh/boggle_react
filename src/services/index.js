import { API } from '../store/general/types'

export const apiAction = ({
  history = {},
  url = '',
  method = 'GET',
  data = null,
  onSuccess = () => {},
  onFailure = () => {},
  label = '',
  headersOverride = null,
}) => {
  return {
    type: API,
    payload: {
      history,
      url,
      method,
      data,
      onSuccess,
      onFailure,
      label,
      headersOverride,
    },
  }
}
