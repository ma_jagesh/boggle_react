import { apiAction } from './index'

const NEW_GAME = 'NEW_GAME'
const GAME_SUMMARY = 'GAME_SUMMARY'

export const requestNewGame = (playerId, history, onSuccess, onFailure) =>
  apiAction({
    history: history,
    url: '/game',
    method: 'POST',
    data: { player_id: playerId },
    onSuccess: onSuccess,
    onFailure: onFailure,
    label: NEW_GAME,
  })

export const submitGameSummary = (gameData, history, onSuccess, onFailure) =>
  apiAction({
    history: history,
    url: '/game/summary',
    method: 'POST',
    data: gameData,
    onSuccess: onSuccess,
    onFailure: onFailure,
    label: GAME_SUMMARY,
  })

export const requestWordValidation = async (userWord, onSuccess, onFailure) => {
  await fetch(`https://montanaflynn-spellcheck.p.rapidapi.com/check/?text=${userWord}`, {
    method: 'GET',
    headers: {
      'x-rapidapi-host': 'montanaflynn-spellcheck.p.rapidapi.com',
      'x-rapidapi-key': '4bf5f57de7mshf70a7a1ef5b93cfp1b7197jsnadc9388ffb33',
    },
  })
    .then((response) => response.json())
    .then((data) => {
      onSuccess(!Object.keys(data.corrections).length > 0)
    })
    .catch((err) => {
      return () => onFailure(err)
    })
}
