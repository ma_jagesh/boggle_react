import { apiAction } from './index'

const FETCH_PLAYERS = 'fetch_players'
const CREATE_PLAYER = 'create_player'

export const requestPlayerCreate = (playerName, history, onSuccess, onFailure) =>
  apiAction({
    history: history,
    url: '/players',
    method: 'POST',
    data: playerName,
    onSuccess: onSuccess,
    onFailure: onFailure,
    label: CREATE_PLAYER,
  })

export const fetchPlayers = (history, onSuccess, onFailure) =>
  apiAction({
    history: history,
    url: '/players',
    onSuccess: onSuccess,
    onFailure: onFailure,
    label: FETCH_PLAYERS,
  })
