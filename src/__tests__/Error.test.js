import { shallow } from 'enzyme'
import React from 'react'
import Error from './../views/Error'
describe('Error', () => {
  it('should match snapshot', () => {
    const component = shallow(<Error />)

    expect(component).toMatchSnapshot()
  })
})
