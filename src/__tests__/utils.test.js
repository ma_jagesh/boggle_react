import { calcTimeLeft } from '../utils/CounterUtils'
import { CreateGridHtml, surroundings, isValidInGrid } from '../utils/GridUtils'
import { successNotify, failureNotify } from '../utils/Notify'
import { toast } from 'react-toastify'
describe('Counter utils', () => {
  it('should display 2 minutes 55 seconds', () => {
    const expectedResult = {
      minutes: '02',
      seconds: 55,
    }
    const result = calcTimeLeft(180000 - 5000)
    expect(result).toMatchObject(expectedResult)
  })

  it('should display 10 minutes', () => {
    const expectedResult = {
      minutes: 10,
      seconds: '00',
    }
    const result = calcTimeLeft(600000)
    expect(result).toMatchObject(expectedResult)
  })

  it('should return 5 seconds', () => {
    const expectedResult = {
      minutes: '00',
      seconds: '05',
    }
    const result = calcTimeLeft(180000 - 175000)
    expect(result).toMatchObject(expectedResult)
  })

  it('should return all zeroes', () => {
    const expectedResult = {
      minutes: '00',
      seconds: '00',
    }
    const result = calcTimeLeft(0)
    expect(result).toMatchObject(expectedResult)
  })
})

describe('Grid utils', () => {
  let expectedMatrix = []
  const characters = 'ZXCVBNMUIOPLAERG'
  beforeEach(() => {
    expectedMatrix = [
      ['Z', 'X', 'C', 'V'],
      ['B', 'N', 'M', 'U'],
      ['I', 'O', 'P', 'L'],
      ['A', 'E', 'R', 'G'],
    ]
  })

  it('should return a grid HTML', () => {
    let expectedHtmlResult = ''
    characters.split('').forEach((char) => {
      expectedHtmlResult += `<div class="cell text-center text-white"> ${char} </div>`
    })

    const [tiles, matrix] = CreateGridHtml(4, 4, characters)
    expect(tiles).toEqual(expectedHtmlResult)
    expect(matrix).toEqual(expectedMatrix)
  })

  it('should return surrounding of a M cell', () => {
    const result = surroundings(expectedMatrix, 1, 2)
    const expectedSurroundingArray = [
      ['L', [2, 3]],
      ['P', [2, 2]],
      ['O', [2, 1]],
      ['C', [0, 2]],
      ['V', [0, 3]],
      ['X', [0, 1]],
      ['U', [1, 3]],
      ['N', [1, 1]],
    ]
    expect(result).toEqual(expectedSurroundingArray)
  })

  it('should return false for invalid path in grid', () => {
    const result = isValidInGrid(expectedMatrix, 'AERM')
    expect(result).toBeFalsy()
  })

  it('should return true for valid path in grid', () => {
    const result = isValidInGrid(expectedMatrix, 'AERO')
    expect(result).toBeTruthy()
  })

  it('should return false for character not in grid', () => {
    const result = isValidInGrid(expectedMatrix, 'DSD')
    expect(result).toBeFalsy()
  })
})

describe('notify success, failure', () => {
  it('should display success message', () => {
    successNotify('this is success message')
    expect(toast.info).toHaveBeenCalled()
  })

  it('should display error message', () => {
    failureNotify('this is success message')
    expect(toast.error).toHaveBeenCalled()
  })
})
