import { shallow } from 'enzyme'
import React from 'react'
import { checkProps, findByTestAttr } from '../utils/TestHelper'
import NewGame from '../views/NewGame'

describe('New game page', () => {
  let component
  const expectedProps = {
    history: {
      location: {
        state: {
          playerId: 123,
        },
      },
    },
    replace: () => {},
  }
  beforeEach(() => {
    component = shallow(<NewGame {...expectedProps} />)
  })

  describe('Checking prop types', () => {
    it('should not throw any warnings', () => {
      const props = checkProps(NewGame, expectedProps)
      expect(props).toBeUndefined()
    })
  })

  it('Should render without errors', () => {
    expect(findByTestAttr(component, 'gameComponent').length).toBe(1)
  })
})
