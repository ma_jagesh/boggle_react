import { shallow } from 'enzyme'
import React from 'react'
import { checkProps } from '../utils/TestHelper'
import Counter from '../components/Counter'

describe('Counter component', () => {
  let component
  beforeEach(() => {
    const props = {
      updateCallback: jest.fn(),
    }
    component = shallow(<Counter {...props} />)
  })

  describe('Checking prop types', () => {
    it('should not throw any warnings', () => {
      const expectedProps = {
        updateCallback: jest.fn(),
      }
      const props = checkProps(Counter, expectedProps)
      expect(props).toBeUndefined()
    })
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })
})
