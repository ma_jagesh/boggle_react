import { shallow } from 'enzyme'
import React from 'react'
import { findByTestAttr, checkProps } from '../utils/TestHelper'
import Summary from '../components/Summary'

describe('Summary page', () => {
  let component
  const expectedProps = {
    score: 190,
    words: ['RUN', 'SUN', 'MOON'],
  }
  beforeEach(() => {
    component = shallow(<Summary {...expectedProps} />)
  })

  describe('Checking prop types', () => {
    it('should not throw any warnings', () => {
      const props = checkProps(Summary, expectedProps)
      expect(props).toBeUndefined()
    })
  })

  it('Shoud match snapshot', () => {
    expect(component).toMatchSnapshot()
  })

  it('Should render without errors', () => {
    expect(findByTestAttr(component, 'SummaryComponent').length).toBe(1)
  })
})
