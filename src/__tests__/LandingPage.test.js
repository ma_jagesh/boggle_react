import { shallow } from 'enzyme'
import React from 'react'
import { findByTestAttr, checkProps } from '../utils/TestHelper'
import LandingPage from '../views/LandingPage'
import thunk from 'redux-thunk'

describe('Landing page', () => {
  let component
  beforeEach(() => {
    const props = {
      history: { push: jest.fn() },
    }
    component = shallow(<LandingPage {...props} />)
  })

  const create = () => {
    const initialState = {
      listData: [],
      player: {},
      formError: '',
      fetchError: '',
    }

    const store = {
      state: initialState,
      getState: jest.fn(() => ({})),
      dispatch: jest.fn(),
    }
    const next = jest.fn()
    const invoke = (action) => thunk(store)(next)(action)
    return { store, next, invoke }
  }

  describe('Checking prop types', () => {
    it('should not throw any warnings', () => {
      const expectedProps = {
        history: {
          push: () => {},
        },
      }
      const props = checkProps(LandingPage, expectedProps)
      expect(props).toBeUndefined()
    })
  })

  it('Should render without errors', () => {
    expect(findByTestAttr(component, 'landingPage').length).toBe(1)
    expect(findByTestAttr(component, 'playerNameInput').length).toBe(1)
    expect(findByTestAttr(component, 'startGameButton').length).toBe(1)
    expect(findByTestAttr(component, 'playersTable').length).toBe(1)
    expect(findByTestAttr(component, 'playerForm').length).toBe(1)
  })

  it('should match snapshot', () => {
    expect(component).toMatchSnapshot()
  })

  it('error changes state', () => {
    const { store, invoke } = create()
    invoke((dispatch, getState) => {
      dispatch('UPDATE_ERRORS')
      getState()
    })

    expect(store.dispatch).toHaveBeenCalledWith('UPDATE_ERRORS')
    expect(store.getState).toHaveBeenCalled()
  })

  it('input error changes state', () => {
    const { store, invoke } = create()
    invoke((dispatch, getState) => {
      dispatch('INPUT_ERROR')
      getState()
    })

    expect(store.dispatch).toHaveBeenCalledWith('INPUT_ERROR')
    expect(store.getState).toHaveBeenCalled()
  })

  it('should submit form successfully', () => {
    const e = { preventDefault: () => {}, stopPropagation: () => {} }
    const form = findByTestAttr(component, 'playerForm')
    expect(form.length).toBe(1)

    form.simulate('submit', e)
  })

  it('should fail submit form', () => {
    const e = { preventDefault: () => {}, stopPropagation: () => {} }
    const wrapper = findByTestAttr(component, 'playerForm')
    expect(wrapper.length).toBe(1)

    wrapper.simulate('submit', e)
  })

  // it('Should emit callback on button click event', () => {
  //   const wrapper = findByTestAttr(component, 'startGameButton')
  //   wrapper.simulate('click')
  //   const callback = mockFunc.mock.calls.length
  //   expect(callback).toBe(1)
  // })
})
