import { toast } from 'react-toastify'

export const successNotify = (successMessage) => {
  toast.info(successMessage)
}

export const failureNotify = (errorMessage) => {
  toast.error(errorMessage)
}
