export const CreateGridHtml = (x, y, characters) => {
  var tiles = []
  var matrix = []
  var rows = []
  for (let i = 0; i < x * y; i++) {
    rows.push(characters.charAt(i))
    if ((i + 1) % 4 === 0) {
      matrix.push(rows)
      rows = []
    }

    tiles.push(`<div class="cell text-center text-white"> ${characters.charAt(i)} </div>`)
  }

  return [tiles.join(''), matrix]
}

const getCell = (matrix, x, y) => {
  const NO_VALUE = null
  let value, hasValue

  try {
    hasValue = matrix[x][y] !== undefined
    value = hasValue ? matrix[x][y] : NO_VALUE
  } catch (e) {
    value = NO_VALUE
  }
  return [value, [x, y]]
}

export const surroundings = (matrix, x, y) => {
  const all = [
    getCell(matrix, x + 1, y + 1),
    getCell(matrix, x + 1, y),
    getCell(matrix, x + 1, y - 1),
    getCell(matrix, x - 1, y),
    getCell(matrix, x - 1, y + 1),
    getCell(matrix, x - 1, y - 1),
    getCell(matrix, x, y + 1),
    getCell(matrix, x, y - 1),
  ]
  return all.filter((record) => record !== null && record[0] !== null)
}

export const isValidInGrid = (matrix, word) => {
  var isValid = false
  const wordArray = word.split('')

  // get all the indexes of the first character of the word
  const indexes = getIndexOfChar(matrix, wordArray[0])

  // loop through the found indexes
  for (let c = 0; c < indexes.length; c++) {
    // get each index
    let [x, y] = indexes[c]
    // if index values are undefined then return from the function
    if (x === undefined || y === undefined) return false

    // loop through each character in the word
    for (let i = 0; i < wordArray.length - 1; i++) {
      // get the indexes, character of the nearby cell
      var neighbours = surroundings(matrix, x, y)
      // loop through neighbour of the cell
      for (let j = 0; j < neighbours.length; j++) {
        // check if the neighbour character is equal
        // to the next character in the word
        if (neighbours[j][0] === wordArray[i + 1]) {
          // if match then set true
          isValid = true
          // get the index of the matched cell
          x = neighbours[j][1][0]
          y = neighbours[j][1][1]
          break
        }
        // if no match then set false
        isValid = false
      }
      // loop continues to the next neighbour

      // if no match found in the neighbour then break the loop
      // no reason to loop through the all the characters in the word
      // since there are no match in the grid
      if (!isValid) break
    }

    // if a match is found then break the entire loop
    if (isValid) break
  }

  // return the isValid variable
  return isValid
}

const getIndexOfChar = (matrix, char) => {
  let x, y
  const indexes = []
  for (let i = 0; i < matrix[0].length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if (matrix[i][j] === char) {
        x = i
        y = j
        indexes.push([x, y])
      }
    }
  }
  return indexes
}
