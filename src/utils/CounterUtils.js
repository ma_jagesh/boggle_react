export const calcTimeLeft = (difference) => {
  // let timeLeft = {}

  if (difference > 0) {
    const min = Math.floor((difference / 1000 / 60) % 60)
    const sec = Math.floor((difference / 1000) % 60)
    return {
      minutes: min > 9 ? min : '0' + min,
      seconds: sec > 9 ? sec : '0' + sec,
    }
  } else {
    return {
      minutes: '00',
      seconds: '00',
    }
  }
}
