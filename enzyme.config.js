import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter(), disableLifecycleMethods: true })

jest.mock('react-redux', () => ({
  useDispatch: () => ({}),
  useSelector: () => false,
}))

jest.mock('react-toastify')
