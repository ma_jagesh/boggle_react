Boggle Game

## Requirements

    Node 13.13.0
    Yarn 1.22.4
    Git 2.21.0

## Installation

Node: [http://nodejs.org/en/download](http://nodejs.org/en/download)

Yarn: `$ npm install -g yarn`

Git: [https://git-scm.com/downloads](https://git-scm.com/downloads)

## Git

Checkout the repository into your choice of directory.

`$ git clone https://bitbucket.org/ma_jagesh/boggle_react.git`

This will create a new folder named **boggle_react**. Navigate inside the folder and open the terminal.

## Running the project

Now that the terminal is running and is pointing at the above created directory, lets install dependencies.

Execute `$ yarn`.

This will install all the necessary dependencies.

In the root directory create a file name `.env`. Inside the file write `REACT_APP_BASE_URL=http://localhost:8000/api`. This is to tell React app where to make API requets.

## Available Scripts

In the project directory, you can run:

### `$ yarn start`

Runs the app in the development mode.<br  />

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br  />

You will also see any lint errors in the console.

### `$ yarn test`

Launches the test runner in the interactive watch mode.<br  />

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
